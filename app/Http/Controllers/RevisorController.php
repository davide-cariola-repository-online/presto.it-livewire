<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Mail\CareerMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('isRevisor')->except('careers');
    }

    public function revisorHome(){
        $article = Article::where('is_accepted', NULL)->first();

        return view('revisor.home', compact('article'));
    }

    public function acceptArticle(Article $article){
        $article->is_accepted = true;
        $article->save();

        return redirect()->back()->with('message', 'Hai accettato l\'annuncio');
    }

    
    public function rejectArticle(Article $article){
        $article->is_accepted = false;
        $article->save();

        return redirect()->back()->with('message', 'Hai rifiutato l\'annuncio');
    }

    public function dashboard(){
        $articles = Article::where('is_accepted', '<>', NULL)->paginate(10);

        return view('revisor.dashboard', compact('articles'));
    }

    public function undoArticle(Article $article){
        $article->is_accepted = NULL;
        $article->save();

        return redirect()->back()->with('message', 'L\'annuncio è nuovamente in revisione');
    }

    public function destroyArticle(Article $article){
        $article->delete();

        return redirect()->back()->with('message', 'L\'annuncio è stato cancellato definitivamente');
    }

    public function careers(){
        Mail::to('admin@presto.it')->send(new CareerMail(Auth::user()));
        return redirect()->back()->with('message', 'Complimenti! Hai richiesto di diventare revisore correttamente');
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:MakeUserRevisor', ["email" => $user->email]);
        return redirect('/')->with('message', 'Complimenti! L\'utente è diventato revisore');
    }
}
