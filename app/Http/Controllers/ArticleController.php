<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index(){
        $articles = Article::where('is_accepted', true)->orderBy('created_at', 'DESC')->paginate(5);

        return view('article.index', compact('articles'));
    }

    public function create(){
        return view('article.create');
    }

    public function show(Article $article){
        return view('article.show', compact('article'));
    }

    public function categoryShow(Category $category){
        return view('article.categoryShow', compact('category'));
    }
}
