<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function homepage(){
        $articles = Article::where('is_accepted', true)->take(6)->orderBy('created_at', 'DESC')->get();
        return view('welcome', compact('articles'));
    }

    public function setLocale($lang){
        session()->put('locale', $lang);
        return redirect()->back();
    }

    public function search(Request $request){
        if(!$request->searched){
            return redirect(route('homepage'))->with('message', 'Inserisci qualcosa nella ricerca');
        }

        $articles = Article::search($request->searched)->where('is_accepted', true)->orderBy('created_at', 'DESC')->paginate(5);

        return view('article.index', compact('articles'));
    }
}
