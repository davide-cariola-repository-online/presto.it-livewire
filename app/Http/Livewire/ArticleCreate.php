<?php

namespace App\Http\Livewire;

use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use App\Models\Article;
use Livewire\Component;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ArticleCreate extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $price;
    public $category;
    public $temporary_images;
    public $images = [];
    public $image;

    protected $rules = [
        'title' => 'required|min:3',
        'price' => 'required|numeric',
        'body' => 'required|min:10',
        'category' => 'required',
        'temporary_images.*' => 'image',
        'images.*' => 'image',
    ];

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function updatedTemporaryImages(){
        if($this->validate([
            'temporary_images.*' => 'image'
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){
        if(in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    // protected function cleanForm(){
    //     $this->title = '';
    //     $this->price = '';
    //     $this->body = '';
    //     $this->category = '';
    // }

    public function articleStore(){
        $this->validate();

        $article = Article::create([
            'title' => $this->title,
            'price' => $this->price,
            'body' => $this->body,
            'category_id' => $this->category,
            'user_id' => Auth::id(),
        ]);

        if(count($this->images)){
            foreach($this->images as $image){
                // $article->images()->create([
                //     'path' => $image->store('images', 'public')
                // ]);
                $newFileName = "articles/{$article->id}";
                $newImage = $article->images()->create([
                    'path' => $image->store($newFileName, 'public')
                ]);
                
                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 400, 300),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                ])->dispatch($newImage->id);
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        $this->reset();
        session()->flash('message', 'Annuncio inserito con successo');
    }

    public function render()
    {
        return view('livewire.article-create');
    }
}
