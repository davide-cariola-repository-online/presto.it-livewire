<x-layout>

    <div class="container-fluid p-5 bg-info shadow text-center">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">Registrati</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8">
                <form method="POST" action="{{ route('register') }}" class="mt-5 p-5 shadow">
                    @csrf
                    
                    <div class="mb-3">
                        <label for="" class="form-label">Username:</label>
                        <input name="name" type="text" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <label for="" class="form-label">Email:</label>
                        <input name="email" type="text" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <label for="" class="form-label">Password:</label>
                        <input name="password" type="password" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <label for="" class="form-label">Password:</label>
                        <input name="password_confirmation" type="password" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <button type="submit" class="btn btn-info">Registrati</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</x-layout>