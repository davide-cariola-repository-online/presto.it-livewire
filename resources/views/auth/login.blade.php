<x-layout>

    <div class="container-fluid p-5 bg-info shadow text-center">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">Accedi</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8">
                <form method="POST" action="{{ route('login') }}" class="mt-5 p-5 shadow">
                    @csrf
                    
                    <div class="mb-3">
                        <label for="" class="form-label">Email:</label>
                        <input name="email" type="text" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <label for="" class="form-label">Password:</label>
                        <input name="password" type="password" class="form-control">
                    </div>
            
                    <div class="mb-3">
                        <button type="submit" class="btn btn-info">Accedi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>