<x-layout>
    <div class="container-fluid p-5 bg-info shadow">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">Esplora la categoria {{$category->name}}</h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            @forelse($category->articles as $article)
                <div class="col-12 col-md-4">
                    <div class="card my-3 shadow">
                        <img src="/media/cover5.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$article->title}}</h5>
                            <p class="card-subtitle">{{$article->price}} E.</p>
                            <p class="card-text">{{$article->body}}</p>
                            <a href="{{route('article.show', compact('article'))}}" class="btn btn-info shadow">Scopri</a>
                            <div class="mt-5 text-center">
                                <span class="card-footer small fst-italic">Inserito da {{$article->user->name}} il {{$article->created_at->format('d/m/Y')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12 col-md-4 text-center">
                    <h2>Non ci sono articoli di questa categoria.</h2>
                </div>
            @endforelse
        </div>
    </div>


</x-layout>