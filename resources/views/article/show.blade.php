<x-layout>
    <div class="container-fluid p-5 bg-info shadow">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">Dettaglio dell'annuncio</h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                      @foreach($article->images as $image)
                        <div class="carousel-item @if($loop->first) active @endif">
                          <img src="{{$image->getUrl(400,300)}}" class="img-fluid w-100" alt="...">
                        </div>
                      @endforeach
                    </div>
                    @if(count($article->images) > 1)
                      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                      </button>
                      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                      </button>
                    @endif
                  </div>
            </div>
            <div class="col-12 col-md-6">
                <h5 class="card-title">{{$article->title}}</h5>
                <p class="card-subtitle">{{$article->price}} E.</p>
                <p class="card-text">{{$article->body}}</p>
                <a class="card-link" href="{{route('categoryShow', ['category' => $article->category])}}"><p>{{$article->category->name}}</p></a>
                <div class="mt-5">
                    <span class="card-footer small fst-italic">Inserito da {{$article->user->name}} il {{$article->created_at->format('d/m/Y')}}</span>
                </div>
            </div>
        </div>
    </div>
</x-layout>