<x-layout>

    <div class="container-fluid p-5 bg-info shadow">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">Aggiungi un annuncio</h1>
            </div>
        </div>
    </div>
    
    <div class="container mt-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8">
                <livewire:article-create>
            </div>
        </div>
    </div>

</x-layout>