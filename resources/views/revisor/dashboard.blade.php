<x-layout>

    @if(count($articles) > 0)

        <div class="container-fluid p-5 bg-info shadow">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <h1 class="display-1">Dashboard</h1>
                </div>
            </div>
        </div>

        @if(session()->has('message'))
            <div class="flex flex-row justify-center my-2 alert alert-success shadow">
                {{session('message')}}    
            </div> 
        @endif

        <div class="container mt-5 text-center">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Titolo</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Utente</th>
                            <th scope="col">Azione</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                                <tr class="@if($article->is_accepted) table-success @else table-danger @endif">
                                    <th scope="row">{{$article->id}}</th>
                                    <td>{{$article->title}}</td>
                                    <td>{{$article->category->name}}</td>
                                    <td>{{$article->user->name}}</td>
                                    <td>
                                        <form class="d-inline" action="{{route('revisor.undoArticle', compact('article'))}}" method="POST">
                                            @csrf
                                            @method('put')
                                            <button type="submit" class="btn btn-warning">Revisiona ancora</button>
                                        </form>
                                        <form class="d-inline" action="{{route('revisor.destroyArticle', compact('article'))}}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Elimina definitivamente</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$articles->links()}}
                </div>
            </div>
        </div>

    @else
        <div class="container-fluid p-5 bg-info shadow">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <h1 class="display-1">Dashboard vuota</h1>
                </div>
            </div>
        </div>
    @endif

</x-layout>