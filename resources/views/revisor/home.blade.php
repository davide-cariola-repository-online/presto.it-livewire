<x-layout>
    
    @if($article)
        <div class="container-fluid p-5 bg-info shadow">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <h1 class="display-1">Revisiona l'annuncio</h1>
                </div>
            </div>
        </div>

        @if(session()->has('message'))
            <div class="flex flex-row justify-center my-2 alert alert-success shadow">
                {{session('message')}}    
            </div> 
        @endif

        <div class="container my-5">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <p class="text-info fw-bold">#{{$article->id}}</p>
                    <h5 class="card-title">{{$article->title}}</h5>
                    <p class="card-subtitle">{{$article->price}} E.</p>
                    <p class="card-text">{{$article->body}}</p>
                    <a class="card-link" href="{{route('categoryShow', ['category' => $article->category])}}"><p>{{$article->category->name}}</p></a>
                    <div class="mt-5">
                        <span class="card-footer small fst-italic">Inserito da {{$article->user->name}} il {{$article->created_at->format('d/m/Y')}}</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row justify-content-center align-items-center text-center">
                @if(count($article->images)>0)
                    @foreach($article->images as $image)
                        <div class="col-12 col-md-6 my-3">
                            <img class="img-fluid" src="{{Storage::url($image->path)}}" alt="">
                        </div>
                        <div class="col-12 col-md-3">
                            <h5>Tags:</h5>
                            @if($image->labels)
                                @foreach($image->labels as $label)
                                    <p class="d-inline">{{$label}},</p>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-12 col-md-3">
                            <h5>Revisione Immagini:</h5>
                            <p>Adulti: <span class="{{$image->adult}}"></span></p>
                            <p>Satira: <span class="{{$image->spoof}}"></span></p>
                            <p>Medicina: <span class="{{$image->medical}}"></span></p>
                            <p>Violenza: <span class="{{$image->violence}}"></span></p>
                            <p>Ammiccante: <span class="{{$image->racy}}"></span></p>
                        </div>
                    @endforeach
                @else
                    <p>Non ci sono immagini</p>
                @endif
            </div>
            <hr>
            <div class="row justify-content-center align-items-center text-center">
                <div class="col-12 col-md-4">
                    <form action="{{route('revisor.acceptArticle', compact('article'))}}" method="POST">
                        @csrf
                        @method('put')
                        <button class="btn shadow btn-success">Accetta</button>
                    </form>
                </div>
                <div class="col-12 col-md-4">
                    <a class="btn btn-warning" href="{{route('revisor.dashboard')}}">Dashboard</a>
                </div>
                <div class="col-12 col-md-4">
                    <form action="{{route('revisor.rejectArticle', compact('article'))}}" method="POST">
                        @csrf
                        @method('put')
                        <button class="btn shadow btn-danger">Rifiuta</button>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid p-5 bg-info shadow">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <h1 class="display-1">Non ci sono annunci da revisionare</h1>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row justify-content-center text-center">
                <div class="col-12 col-md-4">
                    <a class="btn btn-warning" href="{{route('revisor.dashboard')}}">Vai alla Dashboard</a>
                </div>
            </div>
        </div>
    @endif
</x-layout>