<form style="display: inline;" action="{{route('setLocale', $lang)}}" method="post">
    @csrf
    <button type="submit" style="background-color: transparent; border: none;">
        <span class="flag-icon flag-icon-{{$nation}}"></span> 
    </button>
</form>