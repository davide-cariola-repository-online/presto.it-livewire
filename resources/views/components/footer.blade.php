<footer class="container-fluid bg-dark text-white p-5 text-center mt-5">
    <div class="row">
        <div class="col-12">
            <p class="fs-3">Presto.it</p>
            <p class="small">Vuoi lavorare con noi?</p>
            <a href="{{route('revisor.careers')}}"><p class="small">Registrati e clicca qui</p></a>
        </div>
        <hr>
        <div class="col-12">
            <x-_locale lang="it" nation='it' />
            <x-_locale lang="en" nation='gb' />
            <x-_locale lang="es" nation='es' />
        </div>
    </div>
</footer>