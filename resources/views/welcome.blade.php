<x-layout>

    <div class="container-fluid p-5 bg-info shadow">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="display-1">{{__('ui.welcome')}}</h1>
            </div>
        </div>
    </div>

    @if(session()->has('access.denied'))
        <div class="flex flex-row justify-center my-2 alert alert-danger shadow">
            {{session('access.denied')}}    
        </div> 
    @endif

    @if(session()->has('message'))
        <div class="flex flex-row justify-center my-2 alert alert-success shadow">
            {{session('message')}}    
        </div> 
    @endif

    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            @foreach($articles as $article)
                <div class="col-12 col-md-4">
                    <div class="card my-3 shadow">
                        <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(400, 300) : '/media/cover5.jpg'}}" class="img-fluid" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$article->title}}</h5>
                            <p class="card-subtitle">{{$article->price}} E.</p>
                            <p class="card-text">{{$article->body}}</p>
                            <a class="card-link" href="{{route('categoryShow', ['category' => $article->category])}}"><p>{{$article->category->name}}</p></a>
                            <a href="{{route('article.show', compact('article'))}}" class="btn btn-info shadow">Scopri</a>
                            <div class="mt-5 text-center">
                                <span class="card-footer small fst-italic">Inserito da {{$article->user->name}} il {{$article->created_at->format('d/m/Y')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</x-layout>