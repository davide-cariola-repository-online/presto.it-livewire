<div>
    @if(session()->has('message'))
        <div class="flex flex-row justify-center my-2 alert alert-success shadow">
            {{session('message')}}    
        </div> 
    @endif

    <form wire:submit.prevent="articleStore" class="mt-5 p-5 shadow">
        @csrf
        
        <div class="mb-3">
            <label for="" class="form-label">Titolo annuncio:</label>
            <input wire:model="title" type="text" class="form-control @error('title') is-invalid @enderror">
            @error('title') <span class="small text-danger fst-italic">{{$message}}</span>@enderror
        </div>

        <div class="mb-3">
            <label for="" class="form-label">Prezzo:</label>
            <input wire:model="price" type="text" class="form-control @error('price') is-invalid @enderror">
            @error('price') <span class="small text-danger fst-italic">{{$message}}</span>@enderror
        </div>

        <div class="mb-3">
            <label for="" class="form-label">Categoria annuncio:</label>
            <select wire:model.defer="category" class="form-control @error('category') is-invalid @enderror">
                <option value="">Scegli una categoria...</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            @error('category') <span class="small text-danger fst-italic">{{$message}}</span>@enderror
        </div>

        <div class="mb-3">
            <label for="" class="form-label">Descrizione annuncio:</label>
            <textarea wire:model="body" cols="30" rows="15" class="form-control @error('body') is-invalid @enderror"></textarea>
            @error('body') <span class="small text-danger fst-italic">{{$message}}</span>@enderror
        </div>

        <div class="mb-3">
            <input wire:model="temporary_images" type="file" multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Inserisci immagini...">
            @error('temporary_images.*') <span class="small text-danger fst-italic">{{$message}}</span>@enderror
        </div>
        @if(!empty($images))
            <div class="row">
                <div class="col-12">
                    <p>Antemprima immagini:</p>
                    <div class="row border border-4 border-info rounded shadow py-4">
                        @foreach($images as $key => $image)
                            <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}}); background-position:center;"></div>
                            <button type="button" class="btn btn-danger shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">
                                Cancella
                            </button>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="my-3">
            <button type="submit" class="btn btn-info">Crea</button>
        </div>
    </form>
</div>
