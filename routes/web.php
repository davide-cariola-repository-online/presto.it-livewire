<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::post('/locale/{lang}', [PublicController::class, 'setLocale'])->name('setLocale');
Route::get('/article/search', [PublicController::class, 'search'])->name('article.search');

//ARTICLE
Route::get('/article/index', [ArticleController::class, 'index'])->name('article.index');
Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
Route::get('/article/show/{article}', [ArticleController::class, 'show'])->name('article.show');
Route::get('/article/category/{category}', [ArticleController::class, 'categoryShow'])->name('categoryShow');

//REVISOR
Route::get('/revisor/home', [RevisorController::class, 'revisorHome'])->name('revisor.home');
Route::put('/revisor/accept/{article}', [RevisorController::class, 'acceptArticle'])->name('revisor.acceptArticle');
Route::put('/revisor/reject/{article}', [RevisorController::class, 'rejectArticle'])->name('revisor.rejectArticle');
Route::get('/revisor/dashboard', [RevisorController::class, 'dashboard'])->name('revisor.dashboard');
Route::put('/revisor/undo/{article}', [RevisorController::class, 'undoArticle'])->name('revisor.undoArticle');
Route::delete('/revisor/destroy/{article}', [RevisorController::class, 'destroyArticle'])->name('revisor.destroyArticle');
Route::get('/careers', [RevisorController::class, 'careers'])->middleware('auth')->name('revisor.careers');
Route::get('/careers/make-revisor/{user}', [RevisorController::class, 'makeRevisor'])->name('revisor.makeRevisor');